// Word Jumble

// Allows access to iostream from the standard library.
#include <iostream>
//  Allows access to string from the standard library.
#include <string>
// Allows access to cstdlib from the standard library.
#include <cstdlib>
// Allows access to ctime from the standard library.
#include <ctime>

// Allows us to type std:: only once.
using namespace std;

// Our main function
int main()
{
	// Loop that holds the coding for whether a player will want to play again or not.
	bool playAgain = false;

	do
	{
		// Loop that decides what to say at the beginning of the game.
		if (playAgain == true) 
		{
			// What the program says if player chooses to play again.
			cout << "\n\n\t\t Welcome back to Word Jumble!\n\n";
		}
		else 
		{
			// The start of the program.
			cout << "\t\t Welcome to Word Jumble!\n\n";
		}
		// Rules and a little insult to motivate player.
		cout << "You have to unscramble the letters to make a word!\n";
		cout << "Do you really think you're smart enough to figure this out?\n";

		// Holds variable for correct answers.
		int correctAnswers = 0;
		// Loops the program so we play 3 rounds.
		for (int i = 0; i < 3; i++)
		{


			// The basis for my array.
			enum fields { Word, Hint, Num_Fields };
			// How many words/hints in my array.
			const int Num_Words = 10;
			// The beginning function to jumble the words.
			const string Words[Num_Words][Num_Fields] =
			{
				// List of words and hints inside of the array.
				{"sky","Go outside and look up!"},
				{"home","You live here."},
				{"cerberus","Bad doggo!"},
				{"anime","Japanese cartoon."},
				{"controller","Has a lot of buttons"},
				{"gym","Lots of heavy stuff!"},
				{"octopus","Now that's a lot of limbs!"},
				{"cheetah","Fastest animal on land!"},
				{"pewdiepie","How many subscribers do you have?"},
				{"youtube","Never-ending video content."}
			};


			srand(static_cast<unsigned int>(time(0)));

			int choice = (rand() % Num_Words);
			// The word you have to guess
			string theWord = Words[choice][Word];
			// The hint you're given
			string theHint = Words[choice][Hint];

			// The word when it is jumbled
			string jumble = theWord;
			// The size of the jumble.
			int length = jumble.size();

			// Function required to jumble the word.
			for (int i = 0; i < length; ++i)
			{
				int index1 = (rand() % length);
				int index2 = (rand() % length);
				char temp = jumble[index1];
				jumble[index1] = jumble[index2];
				jumble[index2] = temp;
			}
			
			// Displays at the beginning of each round with some tooltips for the player.
			cout << "\nWelcome to round " << i + 1 << " of 3. Good luck you scruffy looking nerf hurder!";
			cout << "\nEnter 'hint' if you can't guess these EASY words and need a hint.";
			cout << "\nEnter 'quit' if you're a quitter and have less than 20 iq.";
			cout << "\nThe jumble is:" << jumble;

			// Holds the variable for guess
			string guess;
			cout << "\n\nYour guess is:";
			// Takes the user's input to answer the question.
			cin >> guess;

			// If the player puts 'quit' as the answer, it closes the program.
			while ((guess != theWord) && (guess != "quit"))
			{
				// If player puts 'hint' as the answer, it will give them a hint.
				if (guess == "hint")
				{
					cout << theHint;
				}
				else
				{
					// Displays if the answer is incorrect.
					cout << "\nSorry, that's not it.";
				}

				cout << "\n\nYour guess:";
				cin >> guess;
			}

			// takes the user's input for the answer.
			if (guess == theWord)
			{
				// Displays if the answer is correct.
				cout << "\nThat's it! You guessed it! Maybe your iq is higher than 20...\n";
				// If the answer is correct adds one to total correct answers.
				correctAnswers++;
			}
			// Tells you your overall score.
			cout << "\n\n\t\t Your score is " << correctAnswers << " out of 3.\n";

			// Displays if player chooses not to play again.
			cout << "\nThank you for playing!";
			cout << "\nMaybe next time you can do better!\n\n";
		}

		// Holds variable for userChoice
		char userChoice;

		// Asks if player wants to play again.
		cout << "\nWould you like to play again? (y/n)\n";
		// Takes user's input for if they would like to play again or not.
		cin >> userChoice;

		// Lets the program read the correct answer for if the player would like to play again or not.
		if (userChoice == 'y' || userChoice == 'Y')
		{
			// Player chose to play again.
			playAgain = true;
			cout << "You chose to play again. Do you love to suffer?";
		}
		else
		{
			// Tells the program the player doesn't want to play again.
			playAgain = false;
		}


	} while (playAgain == true);

	// Displays if player chooses not to play again.
	cout << "\nYou chose not to play again. COWARD!\n";
	// Returns back to main.
	return 0;
}